import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError} from 'rxjs/operators';
import { MarthaService } from './martha.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private _currentUser: User;
  get currentUser(): User{
    return this._currentUser;
  }
  constructor(private martha: MarthaService) {
    this._currentUser = JSON.parse(localStorage.getItem("CU"));
   }

  login(email: string, password: string): Observable<User>{

   return this.martha.select('login', {"username":email, password}).pipe(

      map(users => {

        if(users && users.length === 1)
        {
          const userData = users[0];
  
          this._currentUser = new User(userData.id, userData.username);
          localStorage.setItem('CU', JSON.stringify(this._currentUser));
  
          return this._currentUser;
        }
        else
        {
          return null;
        }
      })
     
      );

    

  }

  
/*
    if (email === 'a@a' && password === 'a'){
        this._currentUser=new User(email);
        localStorage.setItem("CU",JSON.stringify(this._currentUser));

      return this._currentUser;
    }
    else{
      alert('Invalid credentials!')
    }

   /* 5 =="5" // true
      5 === "5" // false
   */
  
  


  logout(){
    this._currentUser = null
    localStorage.setItem("CU",null);
  }
}
