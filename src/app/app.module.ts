import { BrowserModule } from '@angular/platform-browser';
import {  RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { routes } from './app.routes';
import { AppComponent } from './app.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { LoginComponent } from './components/login/login.component';
import { ItemsComponent } from './components/items/items.component';
import { AuthService } from './services/auth.service';
import { AuthGuardService } from './services/auth.guard.service';
import { NavComponent } from './components/nav/nav.component';
import { ItemCreateComponent } from './components/item-create/item-create.component';
import { ItemsService } from './services/items.service';
import { ItemRowComponent } from './components/item-row/item-row.component';
import { ItemDetailComponent } from './components/item-detail/item-detail.component';
import {  HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { MarthaService } from './services/martha.service';
import { AuthInterceptorService } from './services/auth-interceptor.service';

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    LoginComponent,
    ItemsComponent,
    NavComponent,
    ItemCreateComponent,
    ItemRowComponent,
    ItemDetailComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [AuthService, AuthGuardService, ItemsService, MarthaService,
     {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorService, multi: true}
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
