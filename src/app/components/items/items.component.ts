import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { ItemsService } from 'src/app/services/items.service';
import { Item } from 'src/app/models/item.model';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  private items: Item[] = [];
  constructor(private itemsService: ItemsService) {
      itemsService.items.subscribe(items => this.items = items);
      
   }

  itemCreated(newItem: Item)
{
  this.items.push(newItem);
}

  itemDeleted()
  {
    this.itemsService.items.subscribe(items => this.items = items);
  }

  ngOnInit() {
  }

}
