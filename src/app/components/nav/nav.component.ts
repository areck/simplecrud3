import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {


get loggedIn(): boolean{
  return this.authService.currentUser !=null;
}

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
  }
  
  logout(){
    this.authService.logout();
    this.router.navigate(['/']);
  }
}
